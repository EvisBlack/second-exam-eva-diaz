Examen Eva Díaz Perez
 1. Primer commit con el archivo "Readme"
 2. Deploy a PHP Directory Management System connected to a database:
 Para ello, hemos descargado el proyecto php con la base de datos en un directorio llamado /exam2
 En él tenemos 2 carpetas: /dms donde se alojan los archivos php y /sql-file donde está la base de datos
 En la carpeta /exam hemos creado el archivo docker-compose.yml donde daremos las intrucciones para crear los dos contenedores
 y el archivo Dockerfile para el servidor php en el que indicamos la versión de Apache con php que queremos utilizar.
 En el archivo docker-compose.yml hemos indicado que el puerto que queremos que use en nuetro host sea 84(8 y 4 letras de mi primer apellido), que corresponde al puerto :80 del contenedor.
 ports:
            - 84:80
En volumes, hay que poner la ruta completa de nuestro proyecto, en mi caso:
volumes:
            - /home/evis/despliegue/exam2/dms:/var/www/html
Y le hemos asignado una ip con un alias:
networks:
            phpnet2:
                ipv4_address: 192.168.150.200
                aliases: 
                    - phpserver
Para el contenedor de base de datos, hemos especificado la imagen tambien le hemos configurado la red y el volumen con su ruta:
volumes:
            - /home/evis/despliegue/exam2/sql-file:/docker-entrypoint-initdb.d/

        networks:
            phpnet2:
                ipv4_address: 192.168.150.201
                aliases: 
                    - mysqlserver
Y le hemos añadido variables de entorno para la conexión con la base de datos, nombre de la base de datos, usuario y contraseña:
environment:
            MYSQL_ROOT_PASSWORD: admin1234
            MYSQL_DATABASE: dmsdb
            MYSQL_USER: user
            MYSQL_PASSWORD: 1234
Estos datos, lo hemos añadido tambien en el archivo: /dms/admin/include/dbconnectio.php
<?php
$dbuser="user";
$dbpass="1234";
$host="mysqlserver";
$db="dmsdb";
$mysqli =new mysqli($host,$dbuser, $dbpass, $db);
?>
para que nuestra aplicación se pueda conectar correctamente con la base de datos.
Una vez creado el archivo Dockerfile, docker-compose.yml y actualizado el archivo dbconnectio.php; en una terminal en la carpeta raiz, escribimos el siguiente comando:
docker-compose up -d
con esto se levanta nuestro servicio y al entrar en http:\\localhost:84, vemos la aplicación correctamente

4. Add a CI/CD pipeline with 3 stages
Para añadir el pipeline, hemos creado un contenedor con el runner:
docker run -d --name gitlab-runner --restart always \
-v /srv/gitlab-runner/config:/etc/gitlab-runner \
-v /var/run/docker.sock:/var/run/docker.sock \
gitlab/gitlab-runner:latest
He registrado el proyecto de Gitlab:
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
Al hacer esto me ha preguntado la instancia y el token:
https://gitlab.com/
Enter the registration token:
GR1348941Nsbmyx6NKoeyaoaXgcxe
El token lo he obtenido en los settings de Gitlab de mi repositorio en CI/CD
Después me ha pedido un executor: docker
Y una imagen por defecto, he puesto php

Ahora en Gitlab, en la sección CI/CD hemos ido a Pipelines y hemos configurado el archivo para que el pipeline tenga 3 "stages" y en test hemos creado dos scripts que muestran un mensaje de prueba de conexión:
stages:
  - build
  - test
  - deploy
db-test-job:
  stage: test
  script:
    - echo "Test conexión con la base de datos"
proxy-test-job:
  stage: test
  script:
    - echo "Test proxy server"




